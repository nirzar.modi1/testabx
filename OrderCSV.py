import csv

filename = "Order.csv"

headings = ['Entity Name','PAN number','Type of order','Date','AY','FY','Addr','Doc Number','Status of client','Order passed','Amount of Adj','Authority issuing order','Direction passed','Date of hearing','Issues in the order with amount']

with open(filename,mode='w',newline='') as file:
    writer = csv.DictWriter(file,fieldnames=headings)
    writer.writeheader()

print(f'Empty CSV file{filename} is created')