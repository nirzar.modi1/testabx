// import React, { useState, useEffect } from 'react';

// const Dropdown = ({onSelectFile}) => {
//   // State to manage the dropdown visibility
//   const [isOpen, setIsOpen] = useState(false);
  
//   // State to manage the selected value
//   const [selectedValue, setSelectedValue] = useState(null);

//   // Array of options for the dropdown
//   const options = [
//     { label: 'Order', value: 'Order.csv' },
//     { label: 'Notice', value: 'Notice' }
    
//   ];

//   // Function to handle option selection
//   const handleSelect = (option) => {
//     setSelectedValue(option.value);
//     setIsOpen(false);
//   };

//   useEffect(()=>{
//     if(selectedValue){
//       onSelectFile(selectedValue)
//     }
//   },[selectedValue,onSelectFile]);

//   return (
//     <div className="dropdown">
//       <button onClick={() => setIsOpen(!isOpen)} className="dropbtn">
//         {selectedValue ? selectedValue : 'Select an option'}
//       </button>
//       {isOpen && (
//         <div className="dropdown-content">
//           {options.map(option => (
//             <div key={option.value} onClick={() => handleSelect(option)}>
//               {option.label}
//             </div>
//           ))}
//         </div>
//       )}
//     </div>
//   );
// };

// export default Dropdown;

import { useState } from 'react';
const myDropdownData = [
{ id: 1, option: 'Order', file: 'src/Order.csv' },
{ id: 2, option: 'Notice', file: '' }
];

function MyComponent() {
const [selectedOption, setSelectedOption] = useState(null);

const handleDropdownChange = (event) => {
setSelectedOption(parseInt(event.target.value));
};

const selectedOptionObj = myDropdownData.find(item => item.id === selectedOption);
const csvFilePath = selectedOptionObj?.file;
return (
  <div>
  <select onChange={handleDropdownChange}>
  <option value="">--Select an option--</option>
  {myDropdownData.map(item => (
  <option key={item.id} value={item.id}>{item.option}</option>
  ))}
  </select>
  
  {csvFilePath && (
  <a download href={csvFilePath}>Download CSV file</a>
  )}
  </div>
  );
}
export default MyComponent;